'use client';
import { Github, Mail, Twitter, Gitlab, Linkedin, Instagram, Facebook } from 'lucide-react';
import Link from 'next/link';
import { Navigation } from '../components/nav';
import { Card } from '../components/card';

const socials = [
	{
		icon: <Twitter size={20} />,
		href: 'https://twitter.com/kingcaven',
		label: 'Twitter',
		handle: '@kingcaven',
	},
	{
		icon: <Mail size={20} />,
		href: 'mailto:info@cavenmitchell.com.com',
		label: 'Email',
		handle: 'info@cavenmitchell.com',
	},
	{
		icon: <Github size={20} />,
		href: 'https://github.com/cavenmitchell',
		label: 'Github',
		handle: 'cavenmitchell',
	},
	{
		icon: <Gitlab size={20} />,
		href: 'https://gitlab.com/caven',
		label: 'Gitlab',
		handle: 'caven',
	},
	{
		icon: <Linkedin size={20} />,
		href: 'https://linkedin.com/in/caven',
		label: 'Linkedin',
		handle: 'caven',
	},
	{
		icon: <Instagram size={20} />,
		href: 'https://www.instagram.com/cavenmitchell/',
		label: 'Instagram',
		handle: 'cavenmitchell',
	},
	{
		icon: <Facebook size={20} />,
		href: 'https://www.facebook.com/kingcaven',
		label: 'Facebook',
		handle: 'kingcaven',
	},
	
];

export default function Contact() {
	return (
		<div className='bg-custom-green'>
			<Navigation />
			<div className='container flex items-center justify-center min-h-screen px-4 mx-auto pt-24'>
				<div className='grid w-full grid-cols-1 gap-8 mx-auto mt-32 sm:mt-0 sm:grid-cols-3 lg:gap-16'>
					{socials.map((s) => (
						<Card>
							<Link
								href={s.href}
								target='_blank'
								className='p-4 relative flex flex-col items-center gap-4 duration-700 group md:gap-8 md:py-24 lg:pb-48 md:p-16'
							>
								<span className='relative z-10 flex items-center justify-center w-12 h-12 text-sm duration-1000 border rounded-full text-white group-hover:text-white group-hover:bg-zinc-900 border-zinc-500 bg-black group-hover:border-zinc-200 drop-shadow-orange'>
									{s.icon}
								</span>{' '}
								<div className='z-10 flex flex-col items-center'>
									<span className='lg:text-xl font-medium duration-150 xl:text-3xl text-white group-hover:text-white font-display'>
										{s.handle}
									</span>
									<span className='mt-4 text-sm text-center duration-1000 text-white group-hover:text-zinc-200'>
										{s.label}
									</span>
								</div>
							</Link>
						</Card>
					))}
				</div>
			</div>
		</div>
	);
}
