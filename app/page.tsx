import Link from 'next/link';
import Particles from './components/particles';

const navigation = [
  { name: 'Projects', href: '/projects' },
  { name: 'Contact', href: '/contact' },
];

export default function Home() {
  return (
    <div className='flex flex-col items-center justify-center w-screen h-screen overflow-hidden bg-custom-green'>
      <nav className='my-16 z-10 animate-fade-in'>
        <ul className='flex items-center justify-center gap-4'>
          {navigation.map((item) => (
            <Link
              key={item.href}
              href={item.href}
              className='text-md duration-500 text-custom-yellow hover:text-custom-yellow/50'
            >
              {item.name}
            </Link>
          ))}
        </ul>
      </nav>
      <div className='hidden w-screen h-px animate-glow md:block animate-fade-left bg-gradient-to-r from-zinc-300/0 via-zinc-300/50 to-zinc-300/0' />
      <Particles
        className='absolute inset-0 z-9 animate-fade-in'
        quantity={1000}
      />
      <h1 className='py-3.5 px-0.5 z-10 text-4xl text-transparent duration-1000 bg-white cursor-default text-edge-outline animate-title font-display sm:text-6xl md:text-9xl whitespace-nowrap bg-clip-text '>
        Caven Cade Mitchell
      </h1>

      <div className='hidden w-screen h-px animate-glow md:block animate-fade-right bg-gradient-to-r from-white-300/0 via-white-300/50 to-white-300/0' />
      <div className='my-16 px-20 text-center animate-fade-in text-md text-white'>
        <p>I am a technology leader and entrepreneur with extensive experience in software development, community building, and mentoring startups.</p>
        <p>I try my best to balance professional ambition with the responsibilities of family life and my passion for driving innovation in the tech industry.</p>
        <p>Have a project you'd like me to work on? <Link href='/contact' className='text-custom-yellow hover:text-custom-yellow/50'>Contact me.</Link></p>
      </div>
    </div>
  );

}
